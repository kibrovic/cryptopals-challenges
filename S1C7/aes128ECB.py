#!/usr/bin/env python3

import base64
from Crypto.Cipher import AES

with open('7.txt', 'r') as file:
    encrypted_text = base64.b64decode(file.read())

key = b'YELLOW SUBMARINE'
cipher = AES.new(key,AES.MODE_ECB)
plaintext = cipher.decrypt(encrypted_text)

print(plaintext)
