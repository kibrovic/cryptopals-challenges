#!/usr/bin/env python3

import base64

from Crypto.Cipher import AES


def encryptECB(plaintext,key):
    cipher = AES.new(key,AES.MODE_ECB)
    ecb_encrypted_text = cipher.encrypt(plaintext)
    return ecb_encrypted_text

def decryptECB(ciphertext,key):
    cipher = AES.new(key,AES.MODE_ECB)
    ecb_decrypted_text = cipher.decrypt(ciphertext)
    return ecb_decrypted_text

def xorPlainText(text,key):
    xor_bytes = bytes([text[i] ^ key[i] for i in range(0,len(text))])
    return xor_bytes

def padPKCS7(s,k):
    diff = k -len(s)
    result = s + diff*bytes([diff])
    return result

def encryptCBC(plaintext,key,IV):
    chunks = [plaintext[i:i+len(key)] for i in range(0,len(plaintext),len(key))]
    IV = padPKCS7(IV,len(chunks[0]))
    cbc_chunks = []
    for chunk in chunks:
        xored = xorPlainText(chunk,IV)
        cipherblock = encryptECB(xored,key)
        cbc_chunks.append(cipherblock)
        IV = cipherblock
    return b''.join(cbc_chunks)

def decryptCBC(ciphertext,key,IV):
    chunks = [ciphertext[i:i+len(key)] for i in range(0,len(ciphertext),len(key))]
    IV = padPKCS7(IV,len(chunks[0]))
    cbc_chunks = []
    for chunk in chunks:
        cipherblock = decryptECB(chunk,key)
        plaintext = xorPlainText(cipherblock,IV)
        IV = chunk
        cbc_chunks.append(plaintext)
    return b''.join(cbc_chunks)

def main():
    with open('10.txt', 'r') as file:
        encrypted_text = base64.b64decode(file.read().replace('\n', ''))
    key = b"YELLOW SUBMARINE"
    IV = bytes(len(key))
    plaintext = decryptCBC(encrypted_text,key,IV)
    print(plaintext)
    return plaintext

if __name__ == '__main__':
    main()



