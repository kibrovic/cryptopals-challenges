#!/usr/bin/env python3
# credit: https://laconicwolf.com/2018/10/30/cryptopals-challenge-8-detect-ecb-mode-encryption/

def count_repetitions(s,b):
    chunks = [s[i:i+b] for i in range(0,len(s),b)]
    repetitions_no = len(chunks) - len(set(chunks))
    result = {
            'ciphertext': s,
            'repetitions': repetitions_no
            }
    return result

def main():
    encrypted_text = [bytes.fromhex(line.strip()) for line in open('8.txt')]
    blocksize = 16
    repetitions = [count_repetitions(ciphertext,blocksize) for ciphertext in encrypted_text]
    repetitions_sorted = sorted(repetitions, key=lambda x: x["repetitions"], reverse=True)
    print(repetitions_sorted[0])

if __name__ == '__main__':
    main()

